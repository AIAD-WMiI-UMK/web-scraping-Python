# AIAD lab. 12/15 - Web scraping i web crawling w Python

Rozwiązania poniższych zadań (takie lub podobne same zadania jak na Moodle do przedmiotu)
proszę umieszczać w osobnych katalogach, nazwanych odpowiednio `zad_1/`, `zad_2/` itd.

Jeśli w katalogu z rozwiązaniem danego zadania znajduje się więcej niż jeden skrypt Python,
to proszę dodać plik `README.md` (w formacie [Markdown][1], dialekt [GLFM][2]) lub `README` (plik tekstowy)
zawierający informację o tym jak projekt uruchamiać.

**Uwaga:** aby uniknąć konfliktów każdy powinien stworzyć _nową gałąź_,
o takiej samej nazwie jak własna nazwa użytkownika w systemie ($USERNAME),
i się na tą gałąź przełączyć (i na niej wprowadzać zmiany).
Na przykład jeśli moja nazwa użytkownika to `jnareb`, to należy stworzyć
gałąź `jnareb` np. za pomocą
```
$ git switch --create jnareb
```
Rozwiązanie należy zatwierdzić tworząc commit, a następnie opublikować (operacja push).

[1]: https://www.markdownguide.org/getting-started/ "Markdown Guide | Getting Started"
[2]: https://docs.gitlab.com/ee/user/markdown.html "GitLab Flavored Markdown (GLFM)"


-----

## Wydobywanie informacji ze stron internetowych (web scraping)

### Zad. 1 _(archiwum komiksu)_

Pobierz wszystkie rysunki komiksu on-line (jak na przykład [XKCD](https://xkcd.com/)), ewentualnie z ograniczeniem do N ostatnich komiksów, zaczynając od bieżącego i podążając za odnośnikiem do poprzedniego komiksu. Jeśli potrzeba, dodaj numer komiksu lub datę jego utworzenia by nazwy plików po posortowaniu po nazwie układały się w kolejność chronologiczną tworzenia.

Obrazki należy zapisać w osobnym katalogu, tworząc go np. za pomocą biblioteki [`pathlib`](https://docs.python.org/3/library/pathlib.html) jeśli katalog ten nie istnieje. Należy je zapisywać w trybie binarnym (`'wb'`).

### Zad. 2 _(sprawdzanie linków)_

Dla strony o podanym URL, spróbuj pobrać każdą linkowaną stronę (ograniczając się ewentualnie do tylko linków wewnętrznych, lub tylko linków zewnętrznych), i wypisz wszystkie strony / odnośniki zwracające status _HTTP 404 Not Found_. Każdy odnośnik powinien zostać wypisany tylko raz.

### Zad. 3 (top 5)

Znajdź 5 najczęściej pobieranych książek z [Projektu Gutenberg](https://www.gutenberg.org/), korzystając ze strony tego serwisu pod adresem https://www.gutenberg.org/browse/scores/top ; wypisz ich tytuł i identyfikator książki.

### Zad. 4* _(porównywarka cen)_

Pobierz informację o książkach ze strony wybranego wydawnictwa (na przykład ze strony https://www.packtpub.com/all-products/all-books wydawnictwa Packt Publishing Ltd.), ewentualnie przechodząc przez podział na strony. Na podstawie ISBN porównaj cenę z serwisu Packt z ceną na stronie Amazon, lub na stronie Barnes and Noble, lub podobnej.


## Indeksowanie sieci z Scrapy

### Zad. 5 _(Wiki-walk)_

Za pomocą [Scrapy](https://scrapy.org/) przejdź po stronach angielskiej lub polskiej Wikipedii, indeksując je, zaczynając od artykułu poświęconemu językowi Python. Wędrowanie po stronach należy ograniczyć wyłącznie do artykułów, nie przechodząc przy tym do innych wersji językowych. Dla każdego artykułu należy zebrać adres URL, tytuł strony lub jej nagłówek, oraz datę ostatniej modyfikacji. Głębokość zagnieżdżenia ograniczyć za pomocą ustawień Scrapy, dokładniej [`DEPTH_LIMIT`](https://docs.scrapy.org/en/latest/topics/settings.html#depth-limit).

_<u>Dodatkowe zadanie</u>_: zebrać także ilość odnośników wychodzących do innych artykułów Wikipedii, oraz liczbę nagłówków drugiego rzędu (ilość sekcji w spisie treści).


## Pobieranie danych z serwisów internetowych (API)

### Zad. 6 _(geo-lokalizacja adresów IP)_

Dla wybranej strony Wikipedii (na przykład poświęconej [językowi Python](https://en.wikipedia.org/wiki/Python_(programming_language))), i ewentualnie także wszystkich artykułów Wikipedii (bez innych rodzajów stron) linkowanych z danego artykułu, pobierz listę adresów IP osób które edytowały daną stronę. Następnie za pomocą serwisu https://ip-api.com/ lub innego serwisu pozwalającego na geo-lokalizację adresów IP znajdź i wyświetl histogram krajów lub kodów krajów osób edytujących podane artykuły Wikipedii.

### Zad. 7 _(Bugzilla)_

Dla podanej listy identyfikatorów błędów (numerów raportów o błędach), korzystając z [API Bugzilli](http://bugzilla.readthedocs.io/en/latest/api/core/v1/bug.html), znajdź w [Bugzilli dla Eclipse](https://bugs.eclipse.org/bugs/rest/bug) jakim komponentom odpowiadają te błędy, i sprawdź czy i kiedy te błędy zostały naprawione.

Jeśli możliwe, lepiej zrobić to wszystkie używając jednego pobrania danych z serwera Bugzilli.

### Zad. 8 _(GitHub)_

Za pomocą [GitHub REST API](https://docs.github.com/en/rest) (lub modułu [PyGithub](https://pygithub.readthedocs.io/en/latest/introduction.html) który z tego API korzysta) znajdź wszystkie repozytoria które w swoim opisie wymieniają serwer preprintów arXiv.org (a więc zapewne są demonstracją jakiegoś artykułu naukowego).

_<u>Dodatkowe zadanie</u>_: automatycznie wykryj gdy nie wszystkie repozytoria zostały zwrócone ze względu na ograniczenie na maksymalny rozmiar odpowiedzi, i wtedy automatycznie podziel pytanie na zawężone pytania (np. wymagania co do ilości gwiazdek), automatycznie uwzględniając konieczność ograniczenia ilości zapytań na minutę.
